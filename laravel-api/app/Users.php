<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $fillable = ['username', 'id', 'email', 'name', 'role_id'];

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model->id) ){
                $model->id = Str::uuid();
            }
        });
    }
}
